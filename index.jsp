<!DOCTYPE html>
<html>
<head>
    <title>CSS Experiments Home</title>
    <link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
    <h1>CSS Experiments</h1>
    
    <h2>Multi-Column Layout</h2>
    <h3>By width</h3>
    <pre class="CSS">
#columns-width {
    -moz-columns: 20em;
    -webkit-columns: 20em;
    columns: 20em;
}
    </pre>
    <div id="columns-width">
        <%@ include file="cipsum.html" %>
    </div>

    <h3>By number</h3>
    <pre class="CSS">
#columns-number {
    -moz-columns: 3;
    -webkit-columns: 3;
    columns: 3;
}
    </pre>
    <div id="columns-number">
        <%@ include file="cipsum.html" %>
    </div>
</body>
</html>
